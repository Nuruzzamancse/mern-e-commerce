import React from "react";
import "./App.css";
import Index from './views/Index';
import Create from './views/Create';
import Login from './views/Login';
import Signup from './views/Signup';

import { BrowserRouter as Router, Route } from "react-router-dom";


function App() {
  return (
    <Router>
      <div>
        <Route path="/" exact component={Index} />
        <Route path="/create/" component={Create} />
        <Route path="/login/" component={Login} />
        <Route path="/signup/" component={Signup} />
      </div>
    </Router>
  );
}
  export default App;