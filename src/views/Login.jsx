import React from 'react';

import NavBar from '../components/Navbar';
import Card from '../components/Card';

function Login () {
  let body = <h1>This is body</h1>;
  return (
    <div>
      <NavBar />
      <div className="text-center mrgntop">
        <Card body={body}/>
      </div>
    </div>
  );
}

export default Login;
