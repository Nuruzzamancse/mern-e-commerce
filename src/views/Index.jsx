import React, {useState} from 'react';
import {Table, Pagination} from 'react-bootstrap';
import NavBar from '../components/Navbar';
import MoDal from '../components/Modal';
import {Card, Row, Col} from 'react-bootstrap';
import {CSVLink} from 'react-csv';
import {
  MDBCol,
  MDBCard,
  MDBCardBody,
} from 'mdbreact';

function Index () {
  const [show, setShow] = useState (false);
  const [body, setBody] = useState (null);
  const [title, setTitle] = useState (null);

  const handleShow = val => {
    setShow (true);

    setTitle (val.name.first);
    setBody (
      <div className="text-left">
        <Row>
          <Col md="7">
            <Card.Img
              className="mt-2"
              variant="top"
              src={val.image}
              style={{
                height: '30vh',
                marginLeft: 'auto',
                marginRight: 'auto',
              }}
            />
          </Col>
          <Col md="5">
            <div class="mt-5">
              <h4>{val.name.last}</h4>
              <b> Age </b> {val.age} <br />
              <b> Sex </b> {val.sex}
            </div>
          </Col>
        </Row>
      </div>
    );
  };

  const handleClose = () => {
    setShow (false);
  };

  const clicked = () => {
    console.log ('Here');
  };

  let items = [
    {
      name: {first: 'John', last: 'Doe'},
      sex: 'Male',
      age: 42,
      image: 'https://i.ibb.co/Qm7W80w/saju.png',
    },
    {
      name: {first: 'Jane', last: 'Doe'},
      sex: 'Female',
      age: 36,
      image: 'https://i.ibb.co/Qm7W80w/saju.png',
    },
    {
      name: {first: 'Rubin', last: 'Kincade'},
      sex: 'Male',
      age: 73,
      image: 'https://i.ibb.co/Qm7W80w/saju.png',
    },
    {
      name: {first: 'Shirley', last: 'Partridge'},
      sex: 'Female',
      age: 62,
      image: 'https://i.ibb.co/Qm7W80w/saju.png',
    },
    {
      name: {first: 'Shirley', last: 'Partridge'},
      sex: 'Female',
      age: 62,
      image: 'https://i.ibb.co/Qm7W80w/saju.png',
    }
  ];

  return (
    <div className="text-center">
      <NavBar />
      <div className="container">
        <MDBCol className="ml-auto mr-auto mt-3">
          <MDBCard>
          <CSVLink data={items} className="btn btn-success col-md-3 mr-auto ml-auto mt-5">
          Download Contacts
         </CSVLink>
            <MDBCardBody />
            <div>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Image</th>
                  </tr>
                </thead>
                <tbody>
                  {items.map ((item, index) => {
                    return (
                      <tr
                        key={index}
                        className="cursor-pointer"
                        onClick={handleShow.bind (this, item)}
                      >
                        <td>{index + 1}</td>
                        <td>{item.name.first}</td>
                        <td>{item.name.last}</td>
                        <td>
                          <img
                            style={{height: '80px'}}
                            src={item.image}
                            alt={item.age}
                          />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
            <Pagination>
              <Pagination.First onClick={clicked} />
              <Pagination.Prev />
              <Pagination.Item active>{1}</Pagination.Item>
              <Pagination.Item>{2}</Pagination.Item>
              <Pagination.Next />
              <Pagination.Last />
            </Pagination>
          </MDBCard>
        </MDBCol>
        <MoDal
          show={show}
          title={title}
          handleClose={handleClose}
          body={body}
        />

      </div>
    </div>
  );
}

export default Index;
