import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';

import NavBar from '../components/Navbar';

function Signup () {
  return (
    <div>
      <NavBar />
      
      <div className="text-center mrgntop">
        <MDBCol md="4" className="ml-auto mr-auto card-top-margin">
          <MDBCard>
            <MDBCardBody>
            <img
                src="/signup.png"
                className="mb-2"
                alt=""
                style={{height: '80px'}}
              />
              <p className="mb-3 mt-3 small">Keep Your Contact List Safe</p>

              <MDBContainer>
                <MDBRow>
                  <MDBCol md="6" className="mr-auto ml-auto">
                    <form>
                      <p className="h4 text-center mb-4">Sign up</p>
                      <label
                        htmlFor="defaultFormRegisterNameEx"
                        className="grey-text"
                      >
                        Your name
                      </label>
                      <input
                        type="text"
                        id="defaultFormRegisterNameEx"
                        className="form-control"
                      />
                      <br />
                      <label
                        htmlFor="defaultFormRegisterEmailEx"
                        className="grey-text"
                      >
                        Your email
                      </label>
                      <input
                        type="email"
                        id="defaultFormRegisterEmailEx"
                        className="form-control"
                      />
                      <br />
                      <label
                        htmlFor="defaultFormRegisterConfirmEx"
                        className="grey-text"
                      >
                        Confirm your email
                      </label>
                      <input
                        type="email"
                        id="defaultFormRegisterConfirmEx"
                        className="form-control"
                      />
                      <br />
                      <label
                        htmlFor="defaultFormRegisterPasswordEx"
                        className="grey-text"
                      >
                        Your password
                      </label>
                      <input
                        type="password"
                        id="defaultFormRegisterPasswordEx"
                        className="form-control"
                      />
                      <div className="text-center mt-4">
                        <MDBBtn color="mdb-color" type="submit">
                          Register
                        </MDBBtn>
                      </div>
                    </form>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </div>
    </div>
  );
}

export default Signup;
